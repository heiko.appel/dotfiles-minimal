shopt -s extglob

#-----------------------------------------------------------------------------#
have()
{
        unset -v have
        PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin type $1 &>/dev/null &&
                have="yes"
}

have smart &&
_smart()
{
        local cur prev special i

        COMPREPLY=()
        cur=${COMP_WORDS[COMP_CWORD]}
        prev=${COMP_WORDS[COMP_CWORD-1]}

        for (( i=0; i < ${#COMP_WORDS[@]}-1; i++ )); do
                if [[ ${COMP_WORDS[i]} == @(update|install|reinstall|upgrade|remove|check|fix|download|search|query|info|stats|channel|priority|mirror|flag) ]]; then
                        special=${COMP_WORDS[i]}
                fi
        done

        if [ -n "$special" ]; then
                case $special in
                update)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help --after=' -- $cur ) )
                                return 0
                        else
                                # replace by channel search code
                                COMPREPLY=( $( smart channel --show | grep '\[' | tr -d "[]" 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                install|reinstall|remove|fix)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help --stepped \
                                        --urls --download --explain -y --yes' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                upgrade)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help --stepped \
                                        --urls --download --update --check --check-update \
                                        --explain -y --yes' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                check)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help \
                                        --all --installed --available --channels=' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;

                download)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help \
                                        --target= --urls --from-urls' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                stats|search)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                query)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help \
                                          --installed --provides= --requires= --conflicts=        \
                                          --upgrades= --name= --summary= --description=           \
                                          --path= --url= --hide-version --show-summary            \
                                          --show-provides --show-requires --show-prerequires      \
                                          --show-upgrades --show-conflicts --show-providedby      \
                                          --show-requiredby --show-upgradedby --show-conflictedby \
                                          --show-priority --show-channels --show-all --format=    \
                                          --output=' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                info)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help\
                                          --urls --paths' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                channel)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help\
                                          --add --set --remove --show --edit \
                                          --enable --disable -y --yes        \
                                          --help-type=' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                priority|flag)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help\
                                          --set --remove --show --force' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                mirror)
                        if [[ "$cur" == -* ]]; then

                                COMPREPLY=( $( compgen -W '-h --help\
                                          --show --add --remove --remove-all \
                                          --sync= --edit --clear-history     \
                                          --show-penalities' -- $cur ) )
                                return 0
                        else
                                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                                return 0
                        fi
                        ;;
                *)
                        COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
                        return 0
                        ;;

                esac
        fi

        if [[ "$cur" == -* ]]; then

                COMPREPLY=( $( compgen -W '--version  \
                                --config-file= --data-dir= --log-level= \
                                --gui --shell --interface= --ignore-locks' -- $cur ) )
        else

                COMPREPLY=( $( compgen -W 'update install reinstall upgrade \
                                           remove check fix download search \
                                           query info stats channel priority \
                                           mirror flag'  -- $cur ) )

        fi


        return 0
}
[ -n "${have:-}" ] && complete -F _smart $filenames smart
