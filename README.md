# Installation

By running the following command you can install the dotfiles in your current
account

```shell
curl -s https://gitlab.com/heiko.appel/dotfiles-minimal/-/raw/master/bin/dotfiles-install.sh | /bin/bash
```

The script first moves your current dotfiles to a backup folder and then the
files from the repository are installed.

Once the dotfiles are installed, you can update the dotfiles with

```shell
dotfiles pull
```

This pulls the latest changes from the upstream git
repository and installs them in your home directory.

In case you accidentally have overwritten some settings and would like to
return to the original configuration, you can run

```shell
dotfiles reset --hard HEAD
```

# Credits

The management of the dotfiles follows the ideas of

 [https://www.atlassian.com/git/tutorials/dotfiles](https://www.atlassian.com/git/tutorials/dotfiles)
